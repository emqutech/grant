import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '../../../core/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';

@Component({
    selector   : 'fuse-sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class FuseSampleComponent
{
    dataSource: any;
    
 
 
     constructor(
         
     )
     {
         this.dataSource = [
             {
                 no: '1',
                 name: 'MUS PANTOJA SOSA',
                 relationship: 'CONYUGE',
                 birth: '1985-05-02',
                 gender: 'MASCULINO',
                 insurance: '100.00',
                 email: '',
             },
             
         ]
     }
 
 
}
