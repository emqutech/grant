import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';

import { FuseAccountsComponent } from './accounts.component';

const routes = [
    {
        path     : 'accounts',
        component: FuseAccountsComponent
    }
];

@NgModule({
    declarations: [
        FuseAccountsComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports     : [
        FuseAccountsComponent
    ]
})

export class FuseAccountsModule
{
}
