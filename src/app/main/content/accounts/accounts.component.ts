import { Component, ElementRef, OnInit, ViewChild, Inject } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../core/animations';
import { MatPaginator, MatSort } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
    selector   : 'fuse-sample',
    templateUrl: './accounts.component.html',
    styleUrls  : ['./accounts.component.scss'],
    animations : fuseAnimations
})
export class FuseAccountsComponent
{
    dataSource: any;
   

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
    )
    {
        this.dataSource = [
            {
                date: '20/02/2017',
                shared: '5000.00',
                cost1: '5000.00',
                cost2: '0.00',
                cost3: '0.00',
                cost4: '0.00',
                expired_balance: '0,00',
                purchase_price: '0.000000',
                purchased_titles: '0',
                acumulated_titles: '0',
                total_valuation: '0.000'
            },
            {
                date: '20/03/2017',
                shared: '2500.00',
                cost1: '2500.00',
                cost2: '0.00',
                cost3: '0.00',
                cost4: '0.00',
                expired_balance: '0,00',
                purchase_price: '0.901700',
                purchased_titles: '0',
                acumulated_titles: '0',
                total_valuation: '0.000'
            },
        ]
    }


    ngOnInit()
    {
        this.dataSource = [
            {
                date: '20/02/2017',
                shared: '5000.00',
                cost1: '5000.00',
                cost2: '0.00',
                cost3: '0.00',
                cost4: '0.00',
                expired_balance: '0,00',
                purchase_price: '0.000000',
                purchased_titles: '0',
                acumulated_titles: '0',
                total_valuation: '0.000'
            },
            {
                date: '20/03/2017',
                shared: '2500.00',
                cost1: '2500.00',
                cost2: '0.00',
                cost3: '0.00',
                cost4: '0.00',
                expired_balance: '0,00',
                purchase_price: '0.901700',
                purchased_titles: '0',
                acumulated_titles: '0',
                total_valuation: '0.000'
            },
        ]
    }
        
      
}
